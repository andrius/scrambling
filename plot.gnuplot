#!/usr/bin/env gnuplot
set terminal pdf enhanced

set output "mutualinformation.pdf"
set xlabel "t"
set ylabel "Mutual information"
plot "mutualinformation" using 1:2 title "" with lines lw 3, 0 title ""
